import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Search } from "../search";


@Injectable({
  providedIn: "root",
})
export class SearchService {
  url = "http://localhost:3000/search";

  constructor(private http: HttpClient) {}

  getSearches(searchTerm, queryString): Observable<Search> {
    console.log(searchTerm);
    if(queryString == '') {
    return this.http.get<Search>(this.url + "?q=" + searchTerm);
    } else if(!searchTerm.includes('@')) {
      return this.http.get<Search>(this.url + "?username=" + searchTerm);
    } else if(searchTerm.includes('@')) {
      return this.http.get<Search>(this.url + "?email=" + searchTerm);
    }
  }
}
