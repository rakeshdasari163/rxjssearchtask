import { Component } from '@angular/core';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rxjssearch';
  const el = document.getElementById('my-element')!;

const mouseMoves = fromEvent<MouseEvent>(this.el, 'mousemove');

const subscription = this.mouseMoves.subscribe(evt => {
  console.log(`Coords: ${evt.clientX} X ${evt.clientY}`);
  if (evt.clientX < 40 && evt.clientY < 40) {
    this.subscription.unsubscribe();
  }
});
}
